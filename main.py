from constants import constants
from vessel import vessel
from seaState import sinkedSeaMass
from resistanceX import resistanceTable
from plotter import *
import numpy as np
from utility import *
import scipy.integrate as integrate


waterDensity = 1025
g = 9.81
c = constants()
antonia = vessel(c.antoniaInitialSpeed, c.antoniaLightShip, c.antoniaPassengers*75,
                 c.antoniaSuperStructure, c.loa, c.breadth, c.depth, c.ballonDisplacement)
duente = vessel(speed_=c.duenteSpeed, lightship_=c.duenteLightShip)

momentumBefore_y = antonia.momentum()[1] + duente.momentum()[1]
print("ANTONIA SPEED BEFORE COLLISION IS:" + np.array_str(antonia.speed))
print("DUENTE SPEED BEFORE COLLISION IS:" + np.array_str(duente.speed))

antonia_mom_after_y = momentumBefore_y - duente.momentum()[1]*c.momentumMaintance
antonia.speed[1] = antonia_mom_after_y/antonia.displacement()
antonia.superstructure = 0
antonia.payload = 0

duente.speed[1] *= 0.2

print("ANTONIA SPEED AFTER COLLISION IS:" + np.array_str(antonia.speed))
print("DUENTE SPEED AFTER COLLISION IS:" + np.array_str(duente.speed))

#================Sinking Event===============#\

massAdded = sinkedSeaMass(c.windSpeed, c.windDirection, c.currentSpeed,
                          c.currentDirection, c.permability*c.max_displacement, c.seaDepth, 5)
momentum_before_sinking = massAdded.getSpeed(0)*massAdded.mass + antonia.momentum()
antonia.payload += massAdded.mass
antonia.speed = momentum_before_sinking/antonia.displacement()
print("ANTONIA SPEED AFTER SINKING IS:" + np.array_str(antonia.speed))

#============= SINKING Z AXIS =================#
antonia_friction_z = 1.4 * waterDensity * antonia.sectionZ() / 2
antonia_byoancy = ((1-c.permability)*c.max_displacement + antonia.additionalByo*waterDensity)*g


def velocity_z(y, t, cZ, byo, mass):
    # Diff Equasion: u'= -c/m*u^2 - B/m + W/m
    z, u = y
    rhs = (-(u ** 2)*cZ - byo + mass*g)/mass
    dydt = [u, rhs]
    return np.array(dydt)
t = np.arange(0, 12.0, 0.0001)
Zinit = [0, 0]
solZ = integrate.odeint(velocity_z, Zinit, t,
                        args=(antonia_friction_z, antonia_byoancy, antonia.displacement()))
time = 0
tGrounding = 0
tCritical = 0
for s in solZ[:, 0]:
    if s-c.seaDepth <= 0:
        tGrounding = time
    if s-massAdded.criticalHeight <= 0:
        tCritical = time
    time += 0.0001

print("Time of grounding is: "+str(tGrounding))
#============= SINKING Y AXIS =================#
antonia_friction_y = 1.9 * waterDensity * antonia.sectionY() / 2


def velocity_y(y, t, cy, mass, tCritical):
    # Diff Equasion: m*y''= -c*y'^2
    y_axis, u = y
    height = 0
    if t > tCritical:
        height = 10000000
    rhs = -cy/mass*(u-massAdded.getSpeed(height)[1])**2
    dydt = [u, rhs]
    return np.array(dydt)
t_y = np.arange(0, tGrounding, 0.0001)
Yinit = [0, antonia.speed[1]]
solY = integrate.odeint(velocity_y, Yinit, t_y,
                        args=(antonia_friction_y, antonia.displacement(), tCritical))
#============= SINKING X AXIS =================#
resistanceFactor = resistanceTable()


def velocity_x(y, t, mass, tCritical):
    # Diff Equasion: m*y''= -c*y'^2
    x, u = y
    height = 0
    if t > tCritical:
        height = 10000000
    #cx = 1/2*waterDensity*antonia.sectionX()*resistanceFactor.getResistance(u)
    cf= subMergedCt(u,antonia.lengthOA,antonia.depth)*1/2*65.31*waterDensity
    #cf = 0.08* 1 / 2 * antonia.sectionX() * waterDensity
    #print(u - massAdded.getSpeed(height)[0])
    rhs = -cf/mass*(u-massAdded.getSpeed(height)[0])**2
    dydt = [u, rhs]
    return np.array(dydt)

t_x = np.arange(0, tGrounding, 0.0001)
Xinit = [0, antonia.speed[0]]
solX = integrate.odeint(velocity_x, Xinit, t_x,
                        args=(antonia.displacement(), tCritical))

#print(antonia_friction_y)
#print(subMergedCt(1,antonia.lengthOA,antonia.depth))




#plotter(t, solZ, "z(t)")
#plotter(t_y, solY, "y(t)")
#plotter(t_x, solX, "x(t)")
#plott3d(solX[:, 0], solY[:, 0], -solZ[0:len(solX[:, 0]), 0])
#plotResistance(solY, antonia_friction_y, t_y, 0.2)
#plotResistance(solX, 1/2*waterDensity*antonia.sectionX()*resistanceFactor.getResistance(2), t_x)
print(m.sqrt(5.5**2+12.46**2))

