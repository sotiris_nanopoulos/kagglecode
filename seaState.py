import numpy as np
import math as m


class sinkedSeaMass:

    def __init__(self, windSpeed, windDirection, currentSpeed,
                 currentDirection, mass, seaDepth, criticalHeight):
        windSpeed = windSpeed * 3/100
        self.windSpeed = np.array([windSpeed*m.sin(windDirection), windSpeed*m.cos(windDirection), 0])
        self.currentSpeed = np.array([currentSpeed * m.sin(currentDirection), currentSpeed * m.cos(currentDirection), 0])
        self.mass = mass
        self.seaDepth = seaDepth
        self.criticalHeight = criticalHeight

    def getSpeed(self, height):
        if height < self.criticalHeight:
            return self.windSpeed + self.currentSpeed
        else:
            return self.currentSpeed