import numpy as np
import  math as m

class constants(object):

    def __init__(self):

        self.seaDepth = 32.6
        self.currentDirection = m.radians(75)
        self.currentSpeed = 0.514444*0.14
        self.windDirection = m.radians(19)
        self.windSpeed = 0.514444*11.5
        self.collissionAngle = m.radians(55)
        self.momentumMaintance = 0.2

        self.antoniaInitialSpeed = np.array([6,0,0])
        self.antoniaLightShip = 5800
        self.antoniaSuperStructure = 500
        self.antoniaPassengers = 25
        self.loa = 9.36
        self.breadth = 3.12
        self.depth = 1.06
        self.ballonDisplacement = 0.072
        self.max_displacement = 24075.513
        self.permability = 0.5
        duenteSpeedInGeneral = 21
        self.duenteSpeed = np.array([duenteSpeedInGeneral*m.cos(self.collissionAngle),
                                     duenteSpeedInGeneral*m.sin(self.collissionAngle), 0])
        self.duenteLightShip = 6000
        self.waterDensity = 1029

    def getSeaDepth(self):
        return self.seaDepth