import  math as m

waterViscosity = 1.8*10**(-6)

def reynolds( velocity, length):
    return velocity*length/waterViscosity


def cfBasedOnReynolds(velocity, length):
    return  0.075/((m.log10(reynolds(velocity,length))-2)**2)


def subMergedCt(velocity, length,depth):
    return cfBasedOnReynolds(velocity, length)*(1+ 1.5*(depth/length)**1.5 +7*(depth/length)**3)