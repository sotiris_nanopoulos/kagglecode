import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D


def plotter(t, sol, xLabel):
    plt.plot(t, sol[:, 0], 'b', label=xLabel)
    plt.plot(t, sol[:, 1], 'g', label='Velocity (t)')
    plt.legend(loc='best')
    plt.xlabel('t')
    plt.grid()
    plt.show()

def plott3d(x, y, z):
    mpl.rcParams['legend.fontsize'] = 10
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    #solZ[0:len(solX[:, 0])] = -solZ[0:len(solX[:, 0])]
    #ax.plot(solX[:, 0], solY[:, 0], solZ[0:len(solX[:,0]), 0], label='Antonia Movement')
    ax.plot(x, y, z, label='Antonia Movement')
    ax.legend()
    plt.show()


def plotResistance(axis, frictionCoef, time, speed_0=0):
    toPlot = np.zeros(shape=(len(axis[:, 1]), 1))
    i = 0
    for x in axis[:, 1]:
        toPlot[i] = frictionCoef*(x-speed_0) ** 2
        i += 1

    plt.plot(time, toPlot, 'g', label='Resistance(t)')
    plt.legend(loc='best')
    plt.xlabel('t')
    plt.grid()
    plt.show()
