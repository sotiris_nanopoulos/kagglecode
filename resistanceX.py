import numpy as np
from scipy.interpolate import interp1d

class resistanceTable:

    def __init__(self):
        self.x = 0.514444*np.arange(1, 9, 1)
        self.ct = np.array([19.061, 21.54, 25.309, 30.296, 37.988, 47.794, 50.171, 61.148])/(10**3)
        self.f2 = interp1d(self.x, self.ct, kind='cubic')

    def getResistance(self, velocity):
        if velocity < self.x[0]:
            return self.ct[0]
        elif velocity > self.x[7]:
            return self.ct[7]
        else:
            return self.f2(velocity)




