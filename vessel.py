import numpy as np

knotsToMeterPerSec = 0.514444

class vessel:
    additionalByo= 0
    lightShip= 0
    payload= 0
    superstructure= 0
    lengthOA= 0
    breadth= 0
    depth= 0
    speed = np.array([0, 0, 0])

    def __init__(self, speed_=0, lightship_=0, payload_=0,
                 superstructure_=0, loa_=0, breadth_=0,
                 depth_=0, volume_=0):
        self.speed = speed_
        self.additionalByo = volume_
        self.lightShip = lightship_
        self.payload = payload_
        self.superstructure = superstructure_
        self.lengthOA = loa_
        self.breadth = breadth_
        self.depth = depth_
        self.speedToMeterPerSec()

    def speedToMeterPerSec(self):
        self.speed = knotsToMeterPerSec*self.speed

    def displacement(self):
        return self.lightShip+self.payload+self.superstructure

    def momentum(self):
        return self.speed*self.displacement()

    def sectionY(self):
        return self.lengthOA*self.depth

    def sectionZ(self):
        return self.lengthOA*self.depth

    def sectionX(self):
        return self.breadth * self.depth

